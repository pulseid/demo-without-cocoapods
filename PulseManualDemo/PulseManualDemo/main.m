//
//  main.m
//  ThreePartyDemo
//
//  Created by Pulse on 14/9/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
