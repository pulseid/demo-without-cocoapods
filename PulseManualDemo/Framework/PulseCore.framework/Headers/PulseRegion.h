//
//  PulseRegion.h
//  PulseSDK
//
//  Created by Pulse on 26/9/17.
//  Copyright © 2017 com.pulse. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;
#import <PulseCore/PolygonUtil.h>
/**
 Denotes types of regions.
 */
typedef NS_ENUM(NSUInteger, RegionType){
    /**
     Geofence region
     */
    PL_GEOFENCE_REGION = 0,
    /**
     Local Geofence region which clients register in App.
     */
    PL_LOCAL_GEOFENCE_REGION,
    /**
     Beacon region
     */
    PL_BEACON_REGION,
    /**
     Geomagnetic region
     */
    PL_GEOMAGNETIC_REGION,
    /**
     IG region, radius less than 50 meters
     */
    PL_IG_REGION
};

/**
 Denotes types of local geofences.
 Local geofences are registered locally by client apps.
 */
typedef NS_ENUM(NSUInteger, LocalGeofence){
    /**
     Address on File
     */
    LocalGeofenceAOF = 1,
    /**
     Address of Current Residence
     */
    LocalGeofenceACR = 2,
    /**
     Address of Secondary Residence
     */
    LocalGeofenceASR = 3,
    /**
     Address of Work
     */
    LocalGeofenceAOW = 4,
    /**
     Address of Secondary work
     */
    LocalGeofenceASW = 5,
    /**
     Address of Others
     */
    LocalGeofenceAOO = 6
};

/**
 Pulse Region defines regions to monitor.
 */
@interface PulseRegion : NSObject

/**
 An ID represents a region in Pulse Server.
 @note it is not the identifier from CLRegion
 */
@property (nonatomic, strong, nullable) NSString *regionID;
/**
 Type is required when initialising a Pulse Region.
 <pre>
 // a geomagnetic region.
 region.type = PL_GEOMAGNETIC_REGION;
 </pre>
 @see `RegionType` for all types.
 */
@property (nonatomic, assign) enum RegionType type;

/**
 PL_GEOFENCE_REGION
 */
@property (nonatomic, strong, nullable) CLCircularRegion *circularRegion;

/**
 PL_BEACON_REGION
 */
@property (nonatomic, strong, nullable) CLBeaconRegion *beaconRegion;

/**
 Beacon UUID
 @note only for `PL_BEACON_REGION`
 */
@property (nonatomic, strong, nullable) NSString *UUID;
/**
 Beacon Major
 @note only for `PL_BEACON_REGION`
 */
@property (nonatomic, strong, nullable) NSNumber *major;
/**
 Beacon Minor
 @note only for `PL_BEACON_REGION`
 */
@property (nonatomic, strong, nullable) NSNumber *minor;

/**
 Type of Local Geofence
 */
@property (nonatomic, strong, nullable) NSString *localGeofenceType;
/**
 Polygon
 */
@property (nonatomic, strong, nullable) Polygon *polygon;

/**
 Initialise a region with region ID
 @note specify a `RegionType`
 */
+ (nonnull PulseRegion *) regionWithId:(nonnull NSString *)identifier;
/**
 Initialise a region with `CLBeaconRegion`
 @note type is auto assigned as `PL_BEACON_REGION`
 */
+ (nonnull PulseRegion *) regionWithBeaconRegion:(nonnull CLBeaconRegion*)beaconRegion;

+ (nonnull PulseRegion *) regionWithCircularRegion:(nonnull CLCircularRegion*)circularRegion;

/**
 Initialise a venue region with IG Ring or Level Circle

 @param region IG Region of a venue
 @return venue region
 */
+ (nonnull PulseRegion *) venueRegionWithRegion:(nonnull CLCircularRegion*)region;

@end
