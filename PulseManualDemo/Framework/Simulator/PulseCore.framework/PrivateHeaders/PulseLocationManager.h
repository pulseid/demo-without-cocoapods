//  Created by Pulse on 18/12/2014.
//  Copyright (c) 2017 com.pulse. All rights reserved.


#import <Foundation/Foundation.h>
#import <PulseCore/PulseRegion.h>
@import CoreLocation;

/**
 Denotes types of LocationPermission.
 */
typedef NS_ENUM(NSUInteger, LocationPermission){
    PL_LOCATION_UNKNOWN = 0,
    PL_LOCATION_ALWAYS = 1,
    PL_LOCATION_WHENINUSE = 2
};

@protocol PulseLocationManagerDelegate;

@interface PulseLocationManager : NSObject<CLLocationManagerDelegate>

+ (instancetype _Nonnull )sharedClient;
- (void)registerRegion:(PulseRegion*_Nullable)pulseRegion;
- (void)enablePreciseLocation;
- (NSSet*_Nullable)currentMonitoredRegions;
- (void)startSignificantLocationUpdate;
- (void)stopSignificantLocationUpdate;
- (void)stopMonitoringRegions:(NSString*_Nullable)regionType;
- (void)stopGeoRegionsExcept:(NSArray*_Nullable)array;
- (void)triggerGeofenceExit;
- (LocationPermission)locationPermission;
/**
 Check whether a beacon region is already monitored.
 @param regionID identifer for the region in full name. i.e PulseBeacon~UUID~Major~Minor~.
 @return return YES if it is monitored.
 */
- (BOOL)isBeaconMonitoredForExit:(NSString*_Nonnull)regionID;
- (NSString*_Nullable)getUserCurrentLocation;
- (void)stopPreciseLocation;
- (int)numberOfRegionsBeingMonitored:(NSString*_Nonnull)type;
- (CLLocation*_Nullable)getCurrentLocationObj;
+ (NSString*_Nullable)getFormattedLocation:(CLLocation*_Nonnull)location;

    @property (nonatomic, strong) NSThread * _Nonnull thread;
@property (nonatomic) BOOL ranging;
    @property (nonatomic,strong) CLLocationManager * _Nonnull locationManager;
    @property (nonatomic,strong) NSString* _Nullable preciseLocation;
@property (nonatomic, weak, nullable) id<PulseLocationManagerDelegate> delegate;

@end

@protocol PulseLocationManagerDelegate <NSObject>

@required
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager startMonitoringIGRegion:(nonnull PulseRegion *)region;
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager stopMonitoringIGRegion:(nonnull NSString *)identifier;
@optional
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager didFailWithError:(nullable NSError *)error;
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;

@end

