//
//  AppDelegate.h
//  ThreePartyDemo
//
//  Created by Pulse on 14/9/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
@import PulseCore;
@import UserNotifications;
@interface AppDelegate : UIResponder <UIApplicationDelegate, PulseSDKDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UNUserNotificationCenter *center;


@end

